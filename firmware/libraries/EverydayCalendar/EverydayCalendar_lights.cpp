#include "EverydayCalendar_lights.h"
#include <Arduino.h>
#include <SPI.h>
#include <EEPROM.h>

#define UINT16_MSB(uint16) ((uint8_t)(uint16 >> 8))
#define UINT16_LSB(uint16) ((uint8_t)(uint16 & 0xFF))
#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))
#define EEPROM_START_ADR  0x00000000

// EEPROM_PAGE_SIZE defines the span of one calendar page
#define EEPROM_PAGE_SIZE  0x00000030

static const int csPin = 10;
static const int tickle_pin = 9;
static const int outputEnablePin = 8;
// set up the speed, data order and data mode
static SPISettings spiSettings(4000000, MSBFIRST, SPI_MODE0);
static const uint8_t maxBrightness = 255;
static uint8_t brightness = maxBrightness/2;
static uint32_t ledValues[12] = {0}; // Months are the integers in range [0,11], Days are bits within the integers, in range [0,31]

static uint8_t page = 0;

void EverydayCalendar_lights::configure(){
  // LED configurations
  SPI.begin();
  pinMode (csPin, OUTPUT);
  digitalWrite (csPin, HIGH);
  pinMode (tickle_pin, OUTPUT);
  digitalWrite(tickle_pin, HIGH);
  pinMode (outputEnablePin, OUTPUT);
  digitalWrite(outputEnablePin, HIGH);
  // Enable Timer2 interrupt.
  // We want CTC mode (mode 2) where timer resets after compare
  TCCR2A = (TCCR2A & ~0x03) | 0x00;
  TCCR2B = (TCCR2B & ~0x08) | 0x00;
  TCCR2B = (TCCR2B & ~0x07) | 0x02; // selects a clock prescaler of 8. That's a frequency of 31372.55
  OCR2A = brightness; //128
  clearAllLEDs();
}

void EverydayCalendar_lights::begin(){
  TIMSK2 = (1<<OCIE2A) | (1<<TOIE2); // Enable Timers
}

void EverydayCalendar_lights::clearAllLEDs(){
  memset(ledValues, 0, sizeof(ledValues));
}

void EverydayCalendar_lights::setAllLEDs(){
  memset(ledValues, 0xFFFFFFFF, sizeof(ledValues));
}

// Month is in range [0,11]
// Day is in range [0,30]
void EverydayCalendar_lights::setLED(uint8_t month, uint8_t day, bool enable){
  if (month > 11 || day > 30){
    return;
  }

  if (enable){
      ledValues[month] = ledValues[month] | ((uint32_t)1 << day);
  }else{
      ledValues[month] = ledValues[month] & ~((uint32_t)1 << day);
  }
}

void EverydayCalendar_lights::toggleLED(uint8_t month, uint8_t day){
   bool ledState = (*(ledValues+month) & ((uint32_t)1 << (day)));
   setLED(month, day, !ledState);
}

void EverydayCalendar_lights::ResetMemory(){
  for(uint8_t month=0; month<12; month++){
    int addr = EEPROM_START_ADR + (page*EEPROM_PAGE_SIZE) + (month * sizeof(uint32_t));
    const uint32_t zero = 0;
    EEPROM.put(addr, zero);
  }
}

void EverydayCalendar_lights::saveLedStatesToMemory(){
  Serial.println("Save state");
  for(uint8_t month=0; month<12; month++){
    int addr = EEPROM_START_ADR + (page*EEPROM_PAGE_SIZE) + (month * sizeof(uint32_t));
    EEPROM.put(addr, ledValues[month]);
  }
}

void EverydayCalendar_lights::loadLedStatesFromMemory(){
  // If the first month is completely set, then this is the first time we're running the code.
  // Clear the calendar memory
  /*
  uint32_t firstInt;
  EEPROM.get(EEPROM_START_ADR, firstInt);
  if(firstInt == 0xFFFFFFFF){
    Serial.println("First time running everyday calendar.  Clearing all days.");
    for(int i=0; i<sizeof(ledValues); i++){
      EEPROM.write(EEPROM_START_ADR + i, 0x00);
    }
  }
  */
  Serial.print("Loading values of page ");
  Serial.println(page);
  // Load calendar from memory
  for(int i=0; i<sizeof(ledValues); i++){
    *((byte*)ledValues + i) = EEPROM.read(EEPROM_START_ADR + (page*EEPROM_PAGE_SIZE) + i);
  }

  for(int i=0; i<12; i++){
    Serial.print("LED Column ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.println(ledValues[i], HEX);
  }
}

// displays a vertical row of the corresponding column
void EverydayCalendar_lights::displayPage(uint8_t newpage){
  clearAllLEDs();
  ledValues[newpage] = 0xFFFFFFFF;
}

// changes the page and loads the values from the eeprom
void EverydayCalendar_lights::switchPage(uint8_t newpage){
  page = newpage;
  loadLedStatesFromMemory();
}

void EverydayCalendar_lights::setBrightness(uint8_t b){
  if(b > 200){
    b = 200;
  }
  b = ~b;
  if((brightness == 255) && (b != 255)){
    TIMSK2 |= (1<<OCIE2A);
  }
  brightness = b;
  if(brightness == 255){
    TIMSK2 &= ~(1<<OCIE2A);
  }
}


// Code to drive the LED multiplexing.
// This code is called at a very fast period, activating each LED column one by one
// This is done faster than the eye can perceive, and it appears that all twelve columns are illuminated at once.
ISR(TIMER2_COMPA_vect) {
  digitalWrite(outputEnablePin, LOW); // Enable
}
ISR(TIMER2_OVF_vect) {
    static uint16_t activeColumn = 0;
    static byte spiBuf[6];

    digitalWrite(outputEnablePin, HIGH); // Disable

    OCR2A = brightness;

    // "Tickle" the watchdog circuit to keep the LEDs enabled
    digitalWrite(tickle_pin, !digitalRead(tickle_pin));

    // update the next column
    uint16_t month = (1 << activeColumn);
    uint8_t * pDays = (uint8_t*) (ledValues + activeColumn);

    // Send the LED control values into the shift registers
    digitalWrite (csPin, LOW);
    SPI.beginTransaction(spiSettings);
    memcpy(spiBuf, &month, 2);
    spiBuf[2] = pDays[3];
    spiBuf[3] = pDays[2];
    spiBuf[4] = pDays[1];
    spiBuf[5] = pDays[0];
    SPI.transfer(spiBuf, sizeof(spiBuf));
    SPI.endTransaction();
    digitalWrite (csPin, HIGH);

    activeColumn++;
    activeColumn %= 12;
}
