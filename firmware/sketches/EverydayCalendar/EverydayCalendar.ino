#include <EverydayCalendar_lights.h>
#include <EverydayCalendar_touch.h>

// change this to one to display the counter values on button press on serial
// terminal. Leave to 0 to not flood the serial port during debugging
#define SHOW_TOUCH_COUNTER 1

typedef struct {
   int8_t    x;
   int8_t    y;
} Point;

EverydayCalendar_touch cal_touch;
EverydayCalendar_lights cal_lights;
int16_t brightness = 128;

void setup() {
  Serial.begin(9600);
  Serial.println("Sketch setup started");
  
  // Initialize LED functionality
  cal_lights.configure();
  cal_lights.setBrightness(200);
  cal_lights.begin();

  // Perform startup animation
  honeyDrip();

  // Fade out
  for(int b = 200; b >= 0; b--){
    cal_lights.setBrightness(b);
    delay(4);
  }

  // Initialize touch functionality
  cal_touch.configure();
  cal_touch.begin();
  cal_lights.loadLedStatesFromMemory();
  delay(1500);

  // Fade in
  for(int b = 0; b <= brightness; b++){
    cal_lights.setBrightness(b);
    delay(4);
  }
  
  Serial.println("Sketch setup complete");
}

void loop() {
  static Point previouslyHeldButton = {0xFF, 0xFF}; // 0xFF and 0xFF if no button is held
  static uint32_t touchCount = 1;
  static uint8_t sticky_x=0, sticky_y=0, sticky_page = 0;
  static const uint32_t debounceCount = 3;
  static const uint32_t switchPageCount = 30;
  static const uint32_t clearCalendarCount = 300; // ~10 seconds.  This is in units of touch sampling interval ~= 30ms.  
  Point buttonPressed = {0xFF, 0xFF};
  bool touch = cal_touch.scanForTouch();
  
  if (Serial.available() > 4) 
  {
    String teststr = Serial.readString();  //read until timeout
    teststr.trim();
    if (teststr == "clear")
    {
        Serial.println("Clearing Memory");
        cal_lights.ResetMemory();
        cal_lights.loadLedStatesFromMemory();
        
    }   
  }
  // Handle a button press
  if(touch)
  {

    // Brightness Buttons
    if(cal_touch.y == 31){
      if(cal_touch.x == 4){
        brightness -= 3;
      }else if(cal_touch.x == 6){
        brightness += 2;
      }
      brightness = constrain(brightness, 0, 200);
      Serial.print("Brightness: ");
      Serial.println(brightness);
      cal_lights.setBrightness((uint8_t)brightness);
      return;
    }

    // If this button is been held, or it's just starting to be pressed and is the only button being touched
    if(((previouslyHeldButton.x == cal_touch.x) && (previouslyHeldButton.y == cal_touch.y))
    || (debounceCount == 0))
    {
      // Just give a visual indication for now

      // toggle led to acknowledge a button press
      if (touchCount == debounceCount){
        cal_lights.toggleLED((uint8_t)cal_touch.x, (uint8_t)cal_touch.y);
        sticky_x = (uint8_t)cal_touch.x;
        sticky_y = (uint8_t)cal_touch.y;
      }
      // light up the whole column to indicate a page switch
      else if ((touchCount >= switchPageCount) && (touchCount < clearCalendarCount)){ 
        cal_lights.displayPage(cal_touch.x);
        sticky_page = (uint8_t)cal_touch.x;
      }
      // turn on all LEDs to indicate that the calendar is about to get cleared
      else if ((cal_touch.x == 0) && (cal_touch.y == 0) && (touchCount >= clearCalendarCount)){
        cal_lights.setAllLEDs();
        sticky_x = (uint8_t)cal_touch.x;
        sticky_y = (uint8_t)cal_touch.y;
      }

      if(touchCount < 65535){
        touchCount++;
        #if SHOW_TOUCH_COUNTER
        Serial.println(touchCount);
        #endif
      }
    }
  }
  else  // all the buttons are released (again)
  {
    // if the button is pressed for more than 1s (debounced), we have to do something
    if (touchCount >= debounceCount)
    {
      // normal button press (toggle day)
      if ((touchCount >= debounceCount) && (touchCount < switchPageCount)){
        // the led was already switched, now just save it to the EEPROM
        Serial.print("x: ");
        Serial.print(sticky_x);
        Serial.print("\ty: ");
        Serial.println(sticky_y);
        cal_lights.saveLedStatesToMemory();
      }
      else if ((sticky_x == 0) && (sticky_y == 0) && (touchCount >= clearCalendarCount)){
        Serial.println("Resetting all LED states");
        cal_lights.loadLedStatesFromMemory();
        clearAnimation();
      }
      else if (touchCount >= switchPageCount){ 
        // the button was pressed longer than switchPageCount but not yet as long to reset the page
        Serial.println("Switching page");
        cal_lights.switchPage(sticky_page);
      }
    }
    touchCount = 0;
  }

  previouslyHeldButton.x = cal_touch.x;
  previouslyHeldButton.y = cal_touch.y;
}

void honeyDrip(){
  uint16_t interval_ms = 25;
  static const uint8_t monthDayOffset[12] = {0, 3, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0};
  // Turn on all LEDs one by one in the span of a few second
  for(int day = 0; day < 31; day++){
    for(int month = 0; month < 12; month++){
      int8_t adjustedDay = day - monthDayOffset[month];
      if(adjustedDay >= 0 ){
        cal_lights.setLED(month, adjustedDay, true);
      }
    }
    delay(interval_ms);
    interval_ms = interval_ms + 2;
  }
}

void clearAnimation(){
  uint16_t interval_ms = 10;
  uint8_t abort_criteria = 0;
  uint8_t abort_count = 5;
  static const uint8_t monthMaxDay[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  for(int month = 11; month >= 0; month--){
    for(int day = monthMaxDay[month]-1; day >=0; day--){
      cal_lights.setLED(month, day, true);

      // busy delay checking if a button is pressed 
      for (uint8_t i = 0;i<5;i++){
        if(cal_touch.scanForTouch()){
          if (abort_count<abort_criteria){
            abort_count++;
          }
          else{ // Abort clearing and revert led states
            Serial.println("Calendar clearing aborted");
            cal_lights.loadLedStatesFromMemory();
            return;
          }
        }
      }

      cal_lights.setLED(month, day, false);
    }      
  }
  cal_lights.saveLedStatesToMemory();
}
